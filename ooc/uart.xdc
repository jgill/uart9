# White Rabbit clock is 125 MHz
set wr_period 8.000
set wr_half_period  [expr $wr_period / 2.000]

create_clock -period $wr_period -name wrclk -waveform {0.000 4.000} [get_ports clk_ref_i]
set_property HD.CLK_SRC BUFGCTRL_X0Y2 [get_ports clk_ref_i]

# WR CLK
set_input_delay  -clock [get_clocks wrclk] $wr_half_period [get_ports {rst_ref_n_i}]  

set_input_delay  -clock [get_clocks wrclk] $wr_half_period [get_ports {rftime_enable_i}]  
set_input_delay  -clock [get_clocks wrclk] $wr_half_period [get_ports {rftime_inc_i}] 
set_input_delay  -clock [get_clocks wrclk] $wr_half_period [get_ports {rftime_init_i[*]}] 
set_output_delay -clock [get_clocks wrclk] $wr_half_period [get_ports {rftime_o[*]}] 
set_output_delay -clock [get_clocks wrclk] $wr_half_period [get_ports {rftime_locked_o}] 


# Assume that the maximum RF clock - from the DDS is 125 MHz - it may be slower.
set rf_period 8.000
set rf_half_period  [expr $rf_period / 2.000]

create_clock -period $rf_period -name rfclk -waveform {0.000 5.000} [get_ports clk_rf_i]
set_property HD.CLK_SRC BUFGCTRL_X0Y4 [get_ports clk_rf_i]

set_input_delay  -clock [get_clocks rfclk] $wr_half_period [get_ports {rst_rf_n_i}]  
