set projDir [file dirname [info script]]

set zynq       xc7z020-1-clg484
set virtex7    xc7vx690t-2-ffg1761
set ultrascale xcku040-ffva1156-2-e
set device     $zynq

set top xwb_simple_uart

# generics
set hbits     16
set rbits     32


# source files
set srcdir /home/jgill/d3smt/demo-dds/hdl/ip_cores/general-cores/modules/wishbone/wb_uart
read_vhdl /home/jgill/d3smt/demo-dds/hdl/ip_cores/general-cores/modules/genrams/genram_pkg.vhd
read_vhdl /home/jgill/d3smt/demo-dds/hdl/ip_cores/general-cores/modules/wishbone/wishbone_pkg.vhd
read_vhdl $srcdir/uart_async_rx.vhd
read_vhdl $srcdir/uart_async_tx.vhd
read_vhdl $srcdir/uart_baud_gen.vhd
read_vhdl $srcdir/simple_uart_wb.vhd
read_vhdl $srcdir/simple_uart_pkg.vhd
read_vhdl $srcdir/wb_simple_uart.vhd
read_vhdl $srcdir/${top}.vhd

# constraint files
#read_xdc $projDir/${top}.xdc

set start_time [clock seconds]

synth_design -top $top -part $device -mode out_of_context \
             -generic hbits=$hbits -generic rbits=$rbits > synth_${top}.log
write_checkpoint -force synth_${top}

#report_timing_summary -file timing_summary_${top}.rpt
#report_timing -sort_by group -max_paths 100 -path_type full -file timing_${top}.rpt
#report_utilization -hierarchical -file utilization_${top}.rpt
#report_io -file pin_${top}.rpt

set end_time [clock seconds]
set total_time [ expr { $end_time - $start_time} ]
set absolute_time [clock format $total_time -format {%H:%M:%S} -gmt true ]
puts "\ntotal build time: $absolute_time\n"
